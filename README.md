# Desafio

A empresa VirtualCards atua no mercado de cartões de crédito, os seus clientes acessam o site da empresa e preenchem um cadastro para poder solicitar o seu cartão de crédito. Porém nos últimos meses a empresa vem sofrendo ataques de fraude, que são caracterizados por pessoas que utilizam documentos de terceiros para tentar conseguir um cartão de crédito.

O nosso desafio é utilizar modelos de *machine learning* para tentar prever qual o risco de uma solicitação de cartão de crédito ser fraude.

## Orientações:
* O arquivo "Base de dados.xlsx" na guia "HISTORICO" possui o histórico das solicitações que devem ser utilizados como base para o aprendizado do modelo;
* Na guia "DICIONARIO DE DADOS" do arquivo "Base de dados" está o dicionário dos dados;
* Para o teste você pode realizar a combinação de variáveis, criar variáveis, podendo até mesmo utilizar variáveis externas. A sua criatividade é o limite.

## Atividades:
1. Preencher a coluna "PROBABILIDADE" na aba "PREDIÇÃO" do arquivo "Base de dados.xlsx" com a probabilidade que a solicitação tem de ser uma fraude.
2. Gerar a **matriz de confusão** da amostra utilizada para teste.
3. Listar todas as variáveis (mesmo que ela não tenha significância no modelo).
4. Elaborar uma pequena apresentação ilustrando a construção da sua solução para o problema apresentado.

